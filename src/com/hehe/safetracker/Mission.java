package com.hehe.safetracker;

public class Mission {

	private String mission="none",date="2014-00-00",sender="Anonymous",status="0",ID="0",iLat="0",iLon="0";

	public void setmission(String str) {
		this.mission=str;
	}

	public void setmissionDate(String str) {
		this.date=str;
	}
	
	public void setmissionEndLat(String str) {
		this.iLat=str;
	}

	public void setmissionEndLong(String str) {
		this.iLon=str;
	}
	
	public void setmissionSender(String str) {
		this.sender=str;
	}
	
	
	public void setmissionStatus(String str) {
		this.status=str;
	}
	
	public void setmissionID(String str) {
		this.ID=str;
	}


	public String getmission() {
		return this.mission;
	}

	public String getmissionDate( ) {
		return this.date;
	}

	public String getmissionSender( ) {
		return this.sender;
	}
	
	

	public String getmissionStatus( ) {
		return this.status;
	}
	
	
	public String getmissionEndLat() {
		return this.iLat;
	}

	public String getmissionEndLong() {
		return this.iLon;
	}
	
	public String getmissionID( ) {
		return this.ID;
	}


	
}
