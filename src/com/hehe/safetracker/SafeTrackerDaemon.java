package com.hehe.safetracker;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.Service;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.TrafficStats;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.ResultReceiver;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class SafeTrackerDaemon extends Service {
	private NotificationManager Manager; 
	Timer timer=null,sendTimer=null;
	MyTimerTask timerTask;
	sendDataTask send;
	ResultReceiver resultReceiver;
	String imei=" ",bodastatus="on";
	final int NOTIFICATION_ID=1100;
	private static int LockMissCount=0;
	NotificationCompat.Builder mBuilder=null;
	static String  ServerLatitude="0.0",
			ServerLongitude="0.0",
			PrevServerLatitude="0.0",
			PrevServerLongitude="0.0",
			Speed="0.0",
			currentprovider="none",
			SatelliteCount="0",
			SatelliteFixCount="0",
			Accuracy="3000.34",
			GPSdate="000-00-00",
			GPStime="00:00:00";

	WakeLock wakeLock=null;
	static long currentTime=0;
	static int satellitesInFix = 0,stuckcount=0;
	public static boolean  hasGPSStarted=false,hasNetStarted=false,GPStrasmit=false,resetGPSdata=false,isSendLockReleased=false;
	protected LocationManager GPSLocationManager=null,NetLocationManager=null;

	AsyncTask<String, Integer, Double> SendTrackerData;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		_initDaemonInstances();
	}

	private void _initDaemonInstances() {
		if(timer==null) {
			timer= new Timer();
		}

		if(sendTimer==null) {
			sendTimer= new Timer();
		}
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		if(wakeLock==null) {
			wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "stayup");
			if(wakeLock!=null) {
				wakeLock.acquire();
			}
		}
		Log.d("SafeTrackerDaemon", "SafeTrackerDaemon service Created");

		try {
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_COARSE);
			if(GPSLocationManager==null) {
				GPSLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
			}

			GPSLocationManager.addGpsStatusListener(GPSEngineChangeListener);
			GPSLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,10000L, 10.0f, GPSlistener);
			GPSLocationManager.getBestProvider(criteria,true);
			if(NetLocationManager==null) {
				NetLocationManager=(LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
			}
			NetLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,20000L,100.0f, Netlistener);
			NetLocationManager.getBestProvider(criteria,true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			if(Manager==null) {
				Manager=(NotificationManager) getSystemService(NOTIFICATION_SERVICE); 
			}

			if(mBuilder==null) {
				mBuilder =new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.linux);		 
			}

			if(intent!=null) {
				_initDaemonInstances();
				resultReceiver = intent.getParcelableExtra("receiver");
				imei=intent.getStringExtra("imei");
				bodastatus=intent.getStringExtra("status");
				if("on".equalsIgnoreCase(bodastatus)) {
					if(resultReceiver!=null) {					
						try {	
							timerTask = new MyTimerTask();		
							timer.scheduleAtFixedRate(timerTask, 1000,1000);
						}catch(Exception e) {
							e.printStackTrace();
						}

					}else {
						Log.d("SafeTrackerDaemon","resultReceiver is null");

					}

					try {	
						send=new sendDataTask();
						sendTimer.scheduleAtFixedRate(send, 10000,10000);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}else if("off".equalsIgnoreCase(bodastatus)){	
					stopLocationSniff();
					if(timer!=null) {
						timer.cancel();
						timer=null;
					}

					if(sendTimer!=null) {
						sendTimer.cancel();
						sendTimer=null;
					}
				}else {
					Log.e("DAEMON","zombieing the safetracke daemon...");
					onDestroy();
				}
			}else {
				Log.d("SafeTrackerDaemon","intent is null");
			}
			try {
				if(Build.VERSION.SDK_INT>=11) {
					if(bodastatus!=null) {
						mBuilder.setContentTitle(imei+" "+bodastatus.toUpperCase(Locale.ENGLISH));
					}else {
						mBuilder.setContentTitle(imei+" N/A");
						mBuilder.setContentText(imei+" in unkown state.");
					}
					mBuilder.setOngoing(true);
					Manager.notify(NOTIFICATION_ID, mBuilder.build());
					startForeground(NOTIFICATION_ID, mBuilder.build());
				}else {
					startForeground(NOTIFICATION_ID,null);
				}
			}catch(Exception e) {
				e.printStackTrace();
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
		return START_REDELIVER_INTENT;
	}

	private void stopLocationSniff() {
		if(GPSLocationManager!=null) {
			GPSLocationManager.removeGpsStatusListener(GPSEngineChangeListener);
			GPSLocationManager.removeUpdates(GPSlistener);
		}

		if(NetLocationManager!=null) {			
			NetLocationManager.removeUpdates(Netlistener);
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			LockMissCount=0;
			if(Manager!=null) {
				if(mBuilder!=null) {
					mBuilder.setOngoing(false);
				}

				Manager.cancel(NOTIFICATION_ID);
			}


			if(SendTrackerData!=null) {
				Log.e("CANCELLING","send gps data cancelled");
				SendTrackerData.cancel(true);
			}else {
				Log.e("CANCELLING","send gps data was null on cancel");
			}
			if(timer!=null) {	
				timer.cancel();
				timer=null;
			}
			if(sendTimer!=null) {
				sendTimer.cancel();
				sendTimer=null;
			}

			if(send!=null) {
				send.cancel();		
			}

			if(timerTask!=null) {
				timerTask.cancel();				
			}

			if(wakeLock!=null) {
				wakeLock.release();
				wakeLock=null;
			}

		}catch(Exception e) {
			e.printStackTrace();
			super.onDestroy();
		}
		stopLocationSniff();
		Log.d("DESTROY","a world where daemons die too");
	}

	private String getGPStime(long timestamp,String mode) {
		try {
			Date date=new Date(timestamp);
			if("local".equalsIgnoreCase(mode)) {			
				try {		        
					SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
					return df.format(date);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}else {
				try {		        
					SimpleDateFormat df = new SimpleDateFormat("HHmmss");
					return df.format(date);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}	
			}
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private String getGPSDate(long timestamp,String mode) {
		try {
			Date date=new Date(timestamp);
			if("local".equalsIgnoreCase(mode)) {			
				try {		        
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					return df.format(date);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}else {
				try {		        
					SimpleDateFormat df = new SimpleDateFormat("ddMMyy");
					return df.format(date);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}	
			}
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	class MyTimerTask extends TimerTask
	{
		public MyTimerTask() {

			Bundle bundle = new Bundle();
			bundle.putString("status", "background daemon started");
			bundle.putString("lat", "0.0");
			bundle.putString("long", "0.0");
			bundle.putString("speed", "0.0");
			bundle.putString("accuracy", Accuracy);
			bundle.putString("time", GPStime);
			bundle.putString("date", GPSdate);
			bundle.putString("sats", SatelliteCount);
			bundle.putString("fixsats", SatelliteFixCount);
			bundle.putString("lockmiss",String.valueOf(LockMissCount));
			bundle.putString("provider", "none");
			resultReceiver.send(100, bundle);
		}
		@Override
		public void run() {

			if(resetGPSdata) {

				satellitesInFix=0;
				SatelliteCount="0";
				SatelliteFixCount="0";
			}

			String thisGPSdate=getGPSDate(System.currentTimeMillis(),"local");
			String thisGPStime=getGPStime(System.currentTimeMillis(),"local");
			if(thisGPSdate!=null) {
				GPSdate=thisGPSdate;
			}

			if(thisGPStime!=null) {
				GPStime=thisGPStime;
			}
			Bundle bundle = new Bundle();
			bundle.putString("status", "background daemon at work");
			bundle.putString("lat", ServerLatitude);
			bundle.putString("long", ServerLongitude);
			bundle.putString("speed", Speed);
			bundle.putString("sats", SatelliteCount);
			bundle.putString("fixsats", SatelliteFixCount);
			bundle.putString("accuracy", Accuracy);
			bundle.putString("time", GPStime);
			bundle.putString("date", GPSdate);
			bundle.putString("provider",currentprovider);
			bundle.putString("lockmiss",String.valueOf(LockMissCount));
			resultReceiver.send(1, bundle);
			mBuilder.setContentText(getNetStats());
			Manager.notify(NOTIFICATION_ID, mBuilder.build());
		}
	}

	android.location.GpsStatus.Listener GPSEngineChangeListener=new android.location.GpsStatus.Listener() {

		public void onGpsStatusChanged(int event) {
			try {
				int satellites = 0;
				satellitesInFix = 0;
				for (GpsSatellite sat : GPSLocationManager.getGpsStatus(null).getSatellites()) {
					if(sat.usedInFix()) {
						satellitesInFix++;              
					}			
					satellites++;
				}				
				SatelliteCount=String.valueOf(satellites);
				SatelliteFixCount=String.valueOf(satellitesInFix);
			}catch(Exception e) {

			}
		}

	};


	private LocationListener GPSlistener = new LocationListener() {


		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			Log.e("SafeTrackerDaemon", "GPS Location Changed");
			resetGPSdata=false;			
			if (location == null) {
				return;
			}
			try {
				Double thisLat=location.getLatitude();
				Double thisLong=location.getLongitude();
				Float thisSpeed=location.getSpeed();
				Float thisAccuracy=location.getAccuracy();
				currentTime=location.getTime();				
				if(thisAccuracy<500) {
					if(String.valueOf(thisLong).length()>=3&&String.valueOf(thisLat).length()>=3) {
						hasGPSStarted=true;
						GPStrasmit=true;
						ServerLongitude=String.valueOf(thisLong);
						ServerLatitude=String.valueOf(thisLat);			
						Speed=String.valueOf(thisSpeed);
						Accuracy=String.valueOf(thisAccuracy);
						currentprovider="gps";
					}else {
						hasGPSStarted=false;
						GPStrasmit=false;
						currentprovider="none";			
					}
				}else {
					hasGPSStarted=false;	
					GPStrasmit=false;
					currentprovider="none";				
				}
			} catch (Exception e) {
				hasGPSStarted=false;
				GPStrasmit=false;
				currentprovider="none";			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.i("TD info"," GPS has been disable");
			currentprovider="none";
			hasGPSStarted=false;
			GPStrasmit=false;
			resetGPSdata=true;			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Log.i("TD info"," GPS has been enabled");
			resetGPSdata=false;
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			Log.i("TD info"," GPS has been changed to "+status+"...data"+extras.toString());
			if(status==0) {
				GPStrasmit=false;
				hasGPSStarted=false;
				currentprovider="none";			
			}else if(!(String.valueOf(ServerLongitude).length()>=3&&String.valueOf(ServerLongitude).length()>=3)) {
				hasGPSStarted=false;
				currentprovider="none";			

			}else {
				currentprovider="gps";
			}
		}



	};


	private LocationListener Netlistener = new LocationListener() {


		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			Log.e("SafeTrackerDaemon", "NET Location Changed");

			hasNetStarted=true;			
			if (location == null)
				return;
			try {
				Double thisLat=location.getLatitude();
				Double thisLong=location.getLongitude();
				Float thisSpeed=location.getSpeed();
				Float thisAccuracy=location.getAccuracy();
				currentTime=location.getTime();
				if(!GPStrasmit&&thisAccuracy<=1000&&String.valueOf(thisLong).length()>=3&&String.valueOf(thisLat).length()>=3) {
					ServerLongitude=String.valueOf(thisLong);
					ServerLatitude=String.valueOf(thisLat);			
					Speed=String.valueOf(thisSpeed);
					Accuracy=String.valueOf(thisAccuracy);
					currentprovider="net";
				}else {
					currentprovider="none";
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}




		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.i("TD info"," NET has been disable");
			provider="none";
			hasNetStarted=false;
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Log.i("TD info"," NET has been enabled");
			hasNetStarted=true;
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			Log.i("TD info"," NET has been changed to "+status+"...data"+extras.toString());

		}
	};

	private String getGPSData() {
		try {
			String thisGPSdate=getGPSDate(System.currentTimeMillis(),"remote");
			String thisGPStime=getGPStime(System.currentTimeMillis(),"remote");

			JSONObject object = new JSONObject();	
			if(PrevServerLatitude.equals(ServerLatitude)&&PrevServerLongitude.equals(ServerLongitude)&&stuckcount<=6) {
				Log.e("SEND...","boda stuck..."+stuckcount+" times");
				stuckcount++;
				return null;
			}else {
				stuckcount=0;
			}
			if(imei!=null&&imei.length()>=5) {
				object.put("i", imei);
			}else {
				return null;
			}

			if(ServerLongitude!=null&&ServerLongitude.length()>4) {
				PrevServerLongitude=ServerLongitude;
				object.put("lo", ServerLongitude);
			}else {
				return null;
			}
			if(ServerLatitude!=null&&ServerLatitude.length()>4) {
				PrevServerLatitude=ServerLatitude;
				object.put("la", ServerLatitude);
			}else {
				return null;
			}
			if(Speed!=null) {
				object.put("s", Speed);
			}else {
				return null;
			}
			if(thisGPStime!=null) {
				object.put("t", thisGPStime);
			}else {
				return null;
			}
			if(thisGPSdate!=null) {
				object.put("d", thisGPSdate);
			}else {
				return null;
			}
			return object.toString();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String  postLocationData(String TxData,HttpPost httppost) {
		HttpClient httpclient = new DefaultHttpClient();
		// specify the URL you want to post to

		String Response;
		try {
			Log.i("DATA TXed",TxData);
			httppost.setEntity(new StringEntity(TxData, "UTF8"));     
			httppost.addHeader("Content-type", "application/json");
			httppost.addHeader("Accept", "application/json");
			httppost.addHeader("User-Agent", "TrackerDaemon");
			HttpResponse response = httpclient.execute(httppost);
			int httpCode= response.getStatusLine().getStatusCode();
			if(response!=null&&httpCode== HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				Response=EntityUtils.toString(entity); 
				Log.d("RESPONSE TDaemon",Response);
				return Response;
			}
			else {
				Log.d("TrackerDaemon","damn HTTP code..."+httpCode);
				return null;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	public class sendGPSAsynch extends AsyncTask<String, Integer, Double>{
		String data,Response;
		boolean isCancelled=false;
		HttpPost httppost;
		@Override
		protected void onPreExecute() {
			try {
				data=getGPSData();				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onCancelled(){
			isCancelled=true;
			if(httppost!=null) {
				httppost.abort();
			}
		}

		@Override
		protected Double doInBackground(String... params) {
			try {		
				if(isSendLockReleased) {
					isSendLockReleased=false;
					LockMissCount=0;
					httppost = new HttpPost("http://safeboda.com/data/phone");
					Log.e("Buyabe","Send lock OK current missed lock count "+LockMissCount);
					if(data!=null&&data.length()>=12) {				
						Response=postLocationData(data,httppost);
						if(Response!=null) {
							Log.d("SEND RCV",Response);
						}else {
							Log.d("SEND RCV","It was null...server/connection error");
						}

					}else {
						Log.d("SEND TX","no data to send");
					}
				}else {
					LockMissCount++;
					Log.d("Buyabe","Send lock is not released");
				}
				if(isCancelled) {
					Log.e("SEND GPS","it has been cancelled");
					return null;
				}
			}catch(Exception e) {
				e.printStackTrace();

			}
			return null;
		}
		@Override
		protected void onPostExecute(Double result) 
		{
			isSendLockReleased=true;
			if(isCancelled) {
				Log.d("CANCEL GPS","gps has been cancelled");
			}else if(Response!=null) {				
				Log.i("HAHAHA","transmit ok");
			}else {
				Log.d("SHIT LOC","expected response but it was null");
			}
		}

	}

	private int getAppProcessUID() {
		// Get running processes
		int uid =0;		
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> runningProcesses = manager.getRunningAppProcesses();
		if (runningProcesses != null && runningProcesses.size() > 0) {
			for(int p=0;p<runningProcesses.size();p++) {
				if("com.hehe.safetracker".equals(runningProcesses.get(p).processName)) {
					uid=runningProcesses.get(p).uid;				
				}
			}
		}
		return uid;
	}


	private String getNetStats() {
		String received=" ",sent=" ";
		int safeID=getAppProcessUID();
		long rc=TrafficStats.getUidRxBytes(safeID);
		long tx=TrafficStats.getUidTxBytes(safeID);		
		if(rc<=0&&rc<1024L) {
			received=String.valueOf(rc)+"B";
		}else if(1024L<=rc&&rc<1048576L) {
			received=String.valueOf(Math.ceil(rc/1024L))+"KB";
		}else if(1048576L<=rc&&rc<1073741824L) {
			received=String.valueOf(Math.ceil(rc/1048576L))+"MB";
		}else if(1073741824L<=rc&&rc<1099511627776L) {
			received=String.valueOf(Math.ceil(rc/1073741824L))+"GB";
		}else if(rc<=1099511627776L) {
			received=String.valueOf(Math.ceil(rc/1099511627776L))+"TB";
		}else {
			received="0B";
		}

		if(tx<=0&&tx<1024L) {
			sent=String.valueOf(tx)+"B";
		}else if(1024L<=tx&&tx<1048576L) {
			sent=String.valueOf(Math.ceil(tx/1024L))+"KB";
		}else if(1048576L<=tx&&tx<1073741824L) {
			sent=String.valueOf(Math.ceil(tx/1048576L))+"MB";
		}else if(1073741824L<=tx&&tx<1099511627776L) {
			sent=String.valueOf(Math.ceil(tx/1073741824L))+"GB";
		}else if(tx<=1099511627776L) {
			sent=String.valueOf(Math.ceil(tx/1099511627776L))+"TB";
		}else {
			sent="0B";
		}
		return "received: "+received+" sent: "+sent;
	}


	class sendDataTask extends TimerTask
	{
		@Override
		public void run() {
			try {	
				if(SendTrackerData!=null&&SendTrackerData.getStatus() != AsyncTask.Status.FINISHED){
					SendTrackerData.cancel(true);
					Log.e("TIMER KILL","prev send asynch was aborted");
				}else {
					Log.e("TIMER SAVE","prev send asynch was ok for next");
				}
				SendTrackerData =new sendGPSAsynch().execute("");				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}


} 