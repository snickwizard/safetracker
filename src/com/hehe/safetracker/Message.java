package com.hehe.safetracker;

public class Message {

	private String message="no message",date="2014-00-00",sender="Anonymous",senderImage="",status="0",ID="0";

	public void setMessage(String str) {
		this.message=str;
	}

	public void setMessageDate(String str) {
		this.date=str;
	}

	public void setMessageSender(String str) {
		this.sender=str;
	}
	
	public void setMessageSenderImage(String str) {
		this.senderImage=str;
	}

	public void setMessageStatus(String str) {
		this.status=str;
	}
	
	public void setMessageID(String str) {
		this.ID=str;
	}


	public String getMessage() {
		return this.message;
	}

	public String getMessageDate( ) {
		return this.date;
	}

	public String getMessageSender( ) {
		return this.sender;
	}
	
	public String getMessageSenderImage( ) {
		return this.senderImage;
	}

	public String getMessageStatus( ) {
		return this.status;
	}
	
	public String getMessageID( ) {
		return this.ID;
	}


	
}
