package com.hehe.safetracker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Device extends Activity{

	private String phoneNumber=null;
	private String IMEI=null;
	private String isp=null;

	public String  getPhoneNumber(Context appCont) {
		if(phoneNumber==null) {
			TelephonyManager systIDs = (TelephonyManager)appCont.getSystemService(Context.TELEPHONY_SERVICE);
			phoneNumber=systIDs.getLine1Number();				
		}
		return this.phoneNumber;
	}


	@SuppressLint("InlinedApi")
	public String getPhoneIMEI(Context appCont) {
		try {	
			if(IMEI==null) {
				TelephonyInfo DoubleSim=TelephonyInfo.getInstance(appCont);
				if(DoubleSim!=null) {
					if(!DoubleSim.isDualSIM()) {
						TelephonyManager systIDs = (TelephonyManager)appCont.getSystemService(Context.TELEPHONY_SERVICE);
						IMEI=systIDs.getDeviceId();
						if((IMEI==null||IMEI.length()<=5)&&Build.VERSION.SDK_INT>=9) {
							IMEI=Build.SERIAL;
						}
						if(IMEI==null||IMEI.length()<=3) {
							IMEI=getMacAddress(appCont);
						}
						if(IMEI==null||IMEI.length()<=3) {
							IMEI =Build.ID;		
						}
					}else {
						IMEI=DoubleSim.getImeiSIM1();
						if(IMEI==null||IMEI.length()<=10) {
							TelephonyManager systIDs = (TelephonyManager)appCont.getSystemService(Context.TELEPHONY_SERVICE);
							IMEI=systIDs.getDeviceId();
							if(IMEI==null||IMEI.length()<=5&&Build.VERSION.SDK_INT>=9) {
								IMEI=Build.SERIAL;
							}
							if(IMEI==null||IMEI.length()<=3) {
								IMEI=getMacAddress(appCont);
							}
							if(IMEI==null||IMEI.length()<=3) {
								IMEI =Build.ID;		
							}
						}
					}
				}else {
					TelephonyManager systIDs = (TelephonyManager)appCont.getSystemService(Context.TELEPHONY_SERVICE);
					IMEI=systIDs.getDeviceId();
					if((IMEI==null||IMEI.length()<=5)&&Build.VERSION.SDK_INT>=9) {
						IMEI=Build.SERIAL;
					}
					if(IMEI==null||IMEI.length()<=3) {
						IMEI=getMacAddress(appCont);
					}
					if(IMEI==null||IMEI.length()<=3) {
						IMEI =Build.ID;		
					}					
				}
				return this.IMEI;
			}else {
				return IMEI;
			}

		}catch(Exception exc) {
			Log.e("IMEI","damn it "+exc.getMessage());
			return null;
		}
	}

	public String getMacAddress(Context context) {
		WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		String macAddress = wimanager.getConnectionInfo().getMacAddress();
		return macAddress;
	}

	public String getPhoneSerial() {
		if(!Build.SERIAL.isEmpty()) {
			return Build.SERIAL;
		}else {
			if(! Build.DEVICE.isEmpty()) {
				return Build.DEVICE;
			}else {
				return "NOTFOUND";
			}
		}
	}

	public String getPhoneISP(Context appCont) {
		if(isp==null) {
			TelephonyManager systIDs = (TelephonyManager)appCont.getSystemService(Context.TELEPHONY_SERVICE);		
			isp=systIDs.getNetworkOperatorName();
			if(isp!=null&&isp.length()<=2)
				isp=systIDs.getSimOperatorName();
			if(isp!=null&&isp.length()<=2)
				isp=systIDs.getNetworkOperator();
			if(isp!=null&&isp.length()<=2)
				isp=systIDs.getSimOperator();
		}
		return this.isp;
	}
}
