package com.hehe.safetracker;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MissionListAdapter extends BaseAdapter {

	private static ArrayList<Mission> itemDetailsrrayList;
	static Context MissionListContext;
	private LayoutInflater l_Inflater;
	ProgressDialog progress;
	final String HOST = "http://safeboda.com";
	String bodaID="0";
	private int currentPos=-1;
	public MissionListAdapter(String boda,Context safeActivityContext,ArrayList<Mission> results) {
		itemDetailsrrayList = results;
		MissionListContext = safeActivityContext;
		bodaID=boda;
		l_Inflater = LayoutInflater.from(safeActivityContext);
		progress = new ProgressDialog(safeActivityContext);
	}

	@Override
	public int getCount() {
		return itemDetailsrrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return itemDetailsrrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = l_Inflater.inflate(R.layout.mission_layout, null);
			holder = new ViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.shortMessage = (TextView) convertView.findViewById(R.id.mission);
		holder.missionDate = (TextView) convertView.findViewById(R.id.missionDate);
		holder.Accept = (TextView) convertView.findViewById(R.id.missionAccept);
		holder.Deny = (TextView) convertView.findViewById(R.id.missionDeny);
		holder.gotoMission = (ImageView) convertView.findViewById(R.id.missionAction);
		holder.gotoMission.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				getDirectionsOnMap(SafeActivity.latit, SafeActivity.longit,
						itemDetailsrrayList.get(position).getmissionEndLat(),
						itemDetailsrrayList.get(position).getmissionEndLong());
				return true;
			}
		});

		holder.Accept.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				currentPos=position;
				respondMission(itemDetailsrrayList.get(position).getmissionID(),"accept");
				return true;
			}
		});

		holder.Deny.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				currentPos=position;
				respondMission(itemDetailsrrayList.get(position).getmissionID(),"deny");
				return true;
			}
		});

		holder.AuthorName = (TextView) convertView.findViewById(R.id.AuthorName);
		holder.shortMessage.setText(itemDetailsrrayList.get(position).getmission());
		holder.missionDate.setText(itemDetailsrrayList.get(position).getmissionDate());
		holder.AuthorName.setText(itemDetailsrrayList.get(position).getmissionSender());

		convertView.setTag(holder);
		convertView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {

				return true;
			}
		});

		return convertView;
	}

	private void showProgressBar(String message) {
		if (progress != null) {
			progress.setCancelable(false);
			progress.setMessage(message);
			progress.show();
		}
	}

	private void hideProgressBar() {
		if (progress != null) {
			progress.dismiss();
		}
	}

	private void respondMission(final String missionID,final String BodaResponse) {

		AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
			String query="failed";
			String message="connection failed";

			@Override
			protected void onPreExecute() {
				showProgressBar("wait...sending your response.");
			}

			@Override
			protected String doInBackground(String... params) {
				try {
					String response = "";
					HttpURLConnection urlConnection=null;
					try{
						URL url = new URL(params[0]);
						urlConnection = (HttpURLConnection)url.openConnection();
						if(urlConnection!=null) {
							urlConnection.addRequestProperty("User-Agent",bodaID);							
							urlConnection.setConnectTimeout(30000);
						}
						BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
						String line = "";					
						while((line = reader.readLine()) != null){
							response += line + "\n";			
						}               
						if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
							Log.e("Bad Content","Received bad content type while expecting json");
							return null;
						}

						Log.e("SHOOT",response+"\n\nSent\nboda "+bodaID+"\nmissionID "+missionID+"\nresp "+BodaResponse);
						JSONObject obj = new JSONObject(response.toString());	                                
						query=obj.getString("status");					
						message=obj.getString("details");	
					}
					catch (Exception e){					
						e.printStackTrace();
						return null;
					}

					if(urlConnection!=null) {
						urlConnection.disconnect();
					}

					return response;

				} catch (final Exception exc) {

				}
				return null;
			}

			@SuppressWarnings("deprecation")
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					hideProgressBar();

					if("success".equals(query)) {
						SafeActivity.rePopulateListView("mission",currentPos);
					}
					final AlertDialog.Builder	alertbox = new AlertDialog.Builder(MissionListContext); 
					if(query!=null&&query.length()>=3) {
						alertbox.setTitle(query);
					}else {
						alertbox.setTitle("failed");
					}
					if(message!=null&&message.length()>4) {
						alertbox.setMessage(message);
					}else {
						alertbox.setMessage("Error while responding to request.\n\t\t\ttry again.");
					}
					alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {	                               		
						@Override
						public void onClick(DialogInterface arg0, int arg1) {							
							arg0.dismiss();										
						}
					});	
					AlertDialog alert = alertbox.create();
					alert.show();				
					Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
					if (neutral_button != null) {
						neutral_button.setBackgroundDrawable(MissionListContext.getResources()
								.getDrawable(R.drawable.app_button_background));
						neutral_button.setTextColor(MissionListContext.getResources()
								.getColor(android.R.color.white));
					}															
				} catch (final Exception exc) {
					exc.printStackTrace();
				}
			}
		};
		getTask.execute(HOST+"/data/response/mission/"+missionID+"/"+BodaResponse);
	}

	private void getDirectionsOnMap(final String startLat, final String startLong,final String endLat,final String endLog) {		

		AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
			@Override
			protected void onPreExecute() {
				showProgressBar("loading map...");
			}

			@Override
			protected String doInBackground(String... params) {
				try {
					Intent mapIntent = new Intent(MissionListContext, RideOnMap.class);
					mapIntent.putExtra("iLat", startLat);
					mapIntent.putExtra("iLon", startLong);
					mapIntent.putExtra("fLat", endLat);
					mapIntent.putExtra("fLon", endLog);
					MissionListContext.startActivity(mapIntent);		
				} catch (final Exception exc) {
					exc.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					hideProgressBar();
				} catch (final Exception exc) {
					exc.printStackTrace();
				}


			}
		};
		getTask.execute("");
	}

	public void onExitactivity() {
		hideProgressBar();
		System.gc();
	}

	static class ViewHolder {
		TextView AuthorName;
		TextView missionDate;
		TextView Accept;
		TextView Deny;
		TextView shortMessage;
		ImageView gotoMission;
	}
}