package com.hehe.safetracker;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MessageListAdapter  extends BaseAdapter {
	private static ArrayList<Message> itemDetailsrrayList;
	static Context MessageListContext;
	private LayoutInflater l_Inflater;
	ProgressDialog progress;
	final String HOST = "http://safeboda.com";
	String bodaID="0";
	private int currentPos=-1;
	public MessageListAdapter(String boda,Context safeActivityContext, ArrayList<Message> results) {
		itemDetailsrrayList = results;
		bodaID=boda;
		MessageListContext=safeActivityContext;	
		l_Inflater = LayoutInflater.from(safeActivityContext);	
		progress = new ProgressDialog(safeActivityContext);
	}

	@Override
	public int getCount() {
		return itemDetailsrrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return itemDetailsrrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;	
		if (convertView == null) {
			convertView = l_Inflater.inflate(R.layout.message_layout, null);
			holder = new ViewHolder();
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.shortMessage = (TextView) convertView.findViewById(R.id.message);
		holder.messageDate = (TextView) convertView.findViewById(R.id.messageDate);
		holder.messageAuthorIcon = (ImageView) convertView.findViewById(R.id.messageIcon);
		holder.AuthorName = (TextView) convertView.findViewById(R.id.AuthorName);		

		holder.Respond = (TextView) convertView.findViewById(R.id.messageAccept);
		holder.Ignore = (TextView) convertView.findViewById(R.id.messageDeny);

		holder.shortMessage.setText(itemDetailsrrayList.get(position).getMessage());	  
		holder.messageDate.setText(itemDetailsrrayList.get(position).getMessageDate());
		holder.AuthorName.setText(itemDetailsrrayList.get(position).getMessageSender());

		Picasso.with(MessageListContext)
		.load(itemDetailsrrayList.get(position).getMessageSenderImage())
		.error(R.drawable.profile)
		.placeholder(R.drawable.holder)
		.into(holder.messageAuthorIcon);

		convertView.setTag(holder);			
		holder.Respond.setOnLongClickListener(new OnLongClickListener() {         
			@SuppressWarnings("deprecation")
			@Override
			public boolean onLongClick(View v) {
				LinearLayout layout = new LinearLayout(MessageListContext);
				layout.setOrientation(LinearLayout.VERTICAL);

				final EditText Message = new EditText(MessageListContext);
				Message.setMaxLines(2);
				Message.setHint("type message here");
				Message.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);
				layout.addView(Message);
				// Creating and Building the Dialog 
				AlertDialog.Builder builder = new AlertDialog.Builder(MessageListContext);
				builder.setTitle("response to "+itemDetailsrrayList.get(position).getMessageSender());
				builder.setView(layout);
				builder.setPositiveButton("SEND",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,int id) {
						String message=Message.getText().toString();
						if(message!=null&&message.length()>=3) {
							currentPos=position;
							respondMessage(itemDetailsrrayList.get(position).getMessageID(),message);
						}else {
							Toast.makeText(MessageListContext,"Come on Boda!\n Send something valid...",Toast.LENGTH_LONG).show();
						}
						dialog.dismiss();
					}

				});

				builder.setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,int id) {							
						dialog.cancel();
					}
				});	
				AlertDialog alertbox = builder.create();
				alertbox.show();

				//retrieving the button view in order to handle it.
				Button negative_button = alertbox.getButton(DialogInterface.BUTTON_NEGATIVE);


				Button positive_button = alertbox.getButton(DialogInterface.BUTTON_POSITIVE);



				if (positive_button != null) {
					positive_button.setBackgroundDrawable(MessageListContext.getResources()
							.getDrawable(R.drawable.app_button_background));

					positive_button.setTextColor(MessageListContext.getResources()
							.getColor(android.R.color.white));
				}

				if (negative_button != null) {
					negative_button.setBackgroundDrawable(MessageListContext.getResources()
							.getDrawable(R.drawable.app_button_background));

					negative_button.setTextColor(MessageListContext.getResources()
							.getColor(android.R.color.white));
				}
				return true;
			}
		}); 

		holder.Ignore.setOnLongClickListener(new OnLongClickListener() {         

			@Override
			public boolean onLongClick(View v) {
				currentPos=position;
				respondMessage(itemDetailsrrayList.get(position).getMessageID(),"deny");
				return true;
			}
		}); 

		return convertView;
	}


	private void showProgressBar(String message) {
		if (progress != null) {
			progress.setCancelable(false);
			progress.setMessage(message);
			progress.show();
		}
	}


	private void hideProgressBar() {
		if (progress != null) {
			progress.hide();
		}
	}

	private void respondMessage(final String messageID,final String BodaResponse) {
		AsyncTask<String, String, String> getTask = new AsyncTask<String, String, String>() {
			String query="failed";
			String message="connection failed";					
			@Override
			protected void onPreExecute() {
				showProgressBar("wait...sending your response.");
			}

			@Override
			protected String doInBackground(String... params) {
				try {
					String response = "";
					String responseType;
					if("deny".equals(BodaResponse)){
						responseType="deny";
					}else {
						responseType="accept";
					}
					HttpURLConnection urlConnection=null;
					try{
						URL url = new URL(params[0]+responseType);
						urlConnection = (HttpURLConnection)url.openConnection();
						if(urlConnection!=null) {
							urlConnection.addRequestProperty("User-Agent",bodaID);
							if("accept".equals(responseType)){
								urlConnection.addRequestProperty("message",BodaResponse);	
							}
							urlConnection.setConnectTimeout(30000);
						}
						BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
						String line = "";					
						while((line = reader.readLine()) != null){
							response += line + "\n";			
						}               
						if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
							Log.e("Bad Content","Received bad content type while expecting json");
							return null;
						}

						Log.e("SHOOT",response+"\n\nSent\nboda "+bodaID+"\nmessageID "+messageID+"\nresp "+responseType);
						JSONObject obj = new JSONObject(response.toString());	                                
						query=obj.getString("status");					
						message=obj.getString("details");	
					}
					catch (Exception e){					
						e.printStackTrace();
						return null;
					}

					if(urlConnection!=null) {
						urlConnection.disconnect();
					}

					return response;

				} catch (final Exception exc) {

				}
				return null;
			}

			@SuppressWarnings("deprecation")
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					hideProgressBar();
					if("success".equals(query)) {
						SafeActivity.rePopulateListView("message",currentPos);
					}
					final AlertDialog.Builder	alertbox = new AlertDialog.Builder(MessageListContext); 
					if(query!=null&&query.length()>=3) {
						alertbox.setTitle(query);
					}else {
						alertbox.setTitle("failed");
					}
					if(message!=null&&message.length()>4) {
						alertbox.setMessage(message);
					}else {
						alertbox.setMessage("Error while responding to request.\n\t\t\ttry again.");
					}
					alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {	                               		
						@Override
						public void onClick(DialogInterface arg0, int arg1) {

							arg0.dismiss();										
						}
					});	
					AlertDialog alert = alertbox.create();
					alert.show();				
					Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
					if (neutral_button != null) {
						neutral_button.setBackgroundDrawable(MessageListContext.getResources()
								.getDrawable(R.drawable.app_button_background));
						neutral_button.setTextColor(MessageListContext.getResources()
								.getColor(android.R.color.white));
					}															
				} catch (final Exception exc) {
					exc.printStackTrace();
				}
			}
		};
		getTask.execute(HOST+"/data/response/message/"+messageID+"/");
	}


	public void onExitactivity() {

		System.gc();
	}

	static class ViewHolder {
		TextView AuthorName;
		TextView messageDate;		
		TextView shortMessage;
		TextView Respond;		
		TextView Ignore;
		ImageView messageAuthorIcon;		
	}	
}