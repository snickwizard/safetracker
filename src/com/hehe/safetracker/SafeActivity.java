package com.hehe.safetracker;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class SafeActivity extends Activity implements TextToSpeech.OnInitListener{

	private final String HOST="http://safeboda.com";

	private ProgressBar pollPB;

	private Button OnConfirm=null,BusyConfirm=null,OffConfirm=null;
	private ImageView bodaPic=null;
	private TextView BodaName,Latitude,Longitude,Satellites,Accuracy,Speed;
	private TextView MessageTag,MissionTag,GPStime,ConsoleTag;

	private static TextView MsgCount,MissionCount;
	private RelativeLayout Controls,TrackerData;
	private ScrollView Parent;
	static MissionListAdapter ListViewAdapter;
	static MessageListAdapter MessageViewAdapter;

	public static String latit="0.0";

	public static String longit="30.0";
	static String 	NewMessageCount="0";

	static String NewMissionCount="0";

	String SafeBodaName="Unkown";

	String BodaStatus="undefined";
	String OverrideStatus;

	String Lat="NA";

	String Long="NA";

	String Pic=HOST;

	String IMEI="353505820701871";
	static String bodaID="0";
	static Context SafeActivityContext;
	LocationTracker Location;
	Timer updatetimer;
	CountDownTimer expire;
	int ExitCountTap=0;
	Toast TapShow;
	int overrideCount=0;
	static ListView MissionList,MessageList;
	static ArrayList<String> MissionAuthor;
	static ArrayList<String> MissionDate;
	static ArrayList<String> MissionID;
	static ArrayList<String> MissionBody;
	static ArrayList<String> MissionLat;
	static ArrayList<String> MissionLon;

	static ArrayList<String> MessageAuthor;
	static ArrayList<String> MessageAuthorIcon;
	static ArrayList<String> MessageDate;
	static ArrayList<String> MessageID;
	static ArrayList<String> MessageBody;

	private TextToSpeech tts;
	private boolean isttsOK=false;

	AsyncTask<String,String,String> getProfileTask,getMessageTask,BodaOverrider,getBodaStatus;
	@SuppressWarnings("deprecation")


	@Override

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SafeActivityContext=this;
		setContentView(R.layout.activity_safe);
		IMEI=new Device().getPhoneIMEI(SafeActivityContext);

		if(IMEI==null||!IMEI.matches("[0-9]+")) {
			reloadApp("restart");
		}
		OverrideStatus="Busy";
		tts = new TextToSpeech(this, this);
		Parent=(ScrollView)findViewById(R.id.parent);
		pollPB=(ProgressBar)findViewById(R.id.progressBar);
		if(pollPB!=null) {
			pollPB.setIndeterminate(true);
			pollPB.setVisibility(View.INVISIBLE);
		}

		if(OnConfirm==null) {
			OnConfirm=(Button)findViewById(R.id.OnStatus);
		}

		if(BusyConfirm==null) {
			BusyConfirm=(Button)findViewById(R.id.BusyStatus);
		}

		if(OffConfirm==null) {
			OffConfirm=(Button)findViewById(R.id.OffStatus);
		}

		bodaPic=(ImageView)findViewById(R.id.bodaImage);
		bodaPic.setAlpha(0.75f);
		BodaName=(TextView)findViewById(R.id.BodaName);
		TextView IMEIText =(TextView)findViewById(R.id.imei);
		IMEIText.setText(IMEI);
		Controls=(RelativeLayout)findViewById(R.id.Controls);
		TrackerData=(RelativeLayout)findViewById(R.id.bodaLocation);

		Controls.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});

		Latitude=(TextView)findViewById(R.id.bodaLat);
		Longitude=(TextView)findViewById(R.id.bodaLong);
		Satellites=(TextView)findViewById(R.id.sats);
		Accuracy=(TextView)findViewById(R.id.Signal);		
		Speed=(TextView)findViewById(R.id.speed);		
		GPStime=(TextView)findViewById(R.id.bodaTime);	

		MsgCount=(TextView)findViewById(R.id.newMessage);
		MsgCount.setText("0");
		MsgCount.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectConsoleItem("message");		
			}		
		});

		MissionCount=(TextView)findViewById(R.id.newMission);
		MissionCount.setText("0");
		MissionCount.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectConsoleItem("mission");


			}		
		});


		MessageTag=(TextView)findViewById(R.id.MessageTag);
		MessageTag.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectConsoleItem("message");		
			}		
		});

		MessageTag.setOnLongClickListener(new View.OnLongClickListener() {
			public boolean onLongClick(View v) {
				updateBodaMessages("all");								
				return true;
			}
		});


		ConsoleTag=(TextView)findViewById(R.id.ConsoleTag);
		ConsoleTag.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectConsoleItem("console");				
			}		
		});

		MissionTag=(TextView)findViewById(R.id.MissionTag);
		MissionTag.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectConsoleItem("mission");


			}		
		});


		addListenerOnButton();

		LocationTracker Location=new LocationTracker(SafeActivityContext);
		if(!Location.canGetLocation()){                  			
			Location.showSettingsAlert();
		}  

		if(bodaPic!=null){
			bodaPic.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						registerExpirationTimer(5000);
						ExitCountTap++;
						if(ExitCountTap>=10) {
							reloadApp("close");

						}else {
							int remaining=10-ExitCountTap;
							CharSequence toastMessage="Please tap "+remaining+" more times to exit";
							if(TapShow!=null) {
								TapShow.cancel();
							}					
							TapShow = Toast.makeText(SafeActivityContext,toastMessage, Toast.LENGTH_SHORT);
							TapShow.setGravity(Gravity.CENTER, 0, 0);					   
							TapShow.show();

						}
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			});

			bodaPic.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					try {
						reloadApp("restart");
					}catch(Exception e) {
						e.printStackTrace();

					}
					return true;
				}
			});

		}

		if(hasConnection()) {			
			getBobaProfile(HOST+"/boda/"+IMEI+"/0");	
		}else {
			final AlertDialog.Builder	alertbox = new AlertDialog.Builder(SafeActivityContext);            	            
			alertbox.setTitle("failed");
			alertbox.setCancelable(false);
			alertbox.setMessage("Not connected.\nEnable data connection?");
			alertbox.setNeutralButton("RESTART", new DialogInterface.OnClickListener() {	                               		
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
					reloadApp("restart");
				}
			});	

			alertbox.setPositiveButton("ENABLE", new DialogInterface.OnClickListener() {	                               		
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();	
					try {
						ProgressDialog wait =new ProgressDialog(SafeActivityContext);
						wait.setCancelable(false);
						wait.setMessage("Enabling...\nPlease wait.");
						wait.show();

						setMobileDataEnabled(SafeActivityContext,true);						
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchFieldException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					new WaitForRestart().execute("5000");
				}
			});

			alertbox.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {	                               		
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();		
					finish();
				}
			});
			AlertDialog alert = alertbox.create();
			alert.show();
			//retrieving the button view in order to handle it.

			Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
			Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);
			Button negative_button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

			if (neutral_button != null) {
				neutral_button.setBackgroundDrawable(SafeActivityContext.getResources()
						.getDrawable(R.drawable.app_button_background));

				neutral_button.setTextColor(SafeActivityContext.getResources()
						.getColor(android.R.color.white));
			}	

			if (positive_button != null) {
				positive_button.setBackgroundDrawable(SafeActivityContext.getResources()
						.getDrawable(R.drawable.app_button_background));

				positive_button.setTextColor(SafeActivityContext.getResources()
						.getColor(android.R.color.white));
			}	

			if (negative_button != null) {
				negative_button.setBackgroundDrawable(SafeActivityContext.getResources()
						.getDrawable(R.drawable.app_button_background));

				negative_button.setTextColor(SafeActivityContext.getResources()
						.getColor(android.R.color.white));
			}
		}

		MissionList = (ListView)findViewById(R.id.missions);
		MessageList= (ListView)findViewById(R.id.messages);

		selectConsoleItem("console");

		try {	

			updatetimer=new Timer();
			QueryBodaStatusTimerTask poll=new QueryBodaStatusTimerTask();			
			Log.d("TACTAC","entered status poll time");
			updatetimer.scheduleAtFixedRate(poll, 1200000,1200000);


		}catch(Exception e) {
			e.printStackTrace();
		}
	}



	private void selectConsoleItem(String item) {
		try {
			sendScroll();
			if("console".equals(item)){
				MissionList.setVisibility(View.GONE);
				MessageList.setVisibility(View.GONE);
				TrackerData.setVisibility(View.VISIBLE);
				ConsoleTag.setTextColor(Color.argb(200, 255, 255,255));		
				MissionTag.setTextColor(Color.argb(255, 73, 86,94));
				MessageTag.setTextColor(Color.argb(255, 73, 86,94));
			}else if("message".equals(item)){
				MissionList.setVisibility(View.GONE);
				MessageList.setVisibility(View.VISIBLE);
				MessageTag.setTextColor(Color.argb(200, 255, 255,255));		
				MissionTag.setTextColor(Color.argb(255, 73, 86,94));
				ConsoleTag.setTextColor(Color.argb(255, 73, 86,94));
				TrackerData.setVisibility(View.GONE);
			}else {
				MissionList.setVisibility(View.VISIBLE);
				MissionTag.setTextColor(Color.argb(200, 255, 255,255));		
				MessageTag.setTextColor(Color.argb(255, 73, 86,94));
				ConsoleTag.setTextColor(Color.argb(255, 73, 86,94));
				MessageList.setVisibility(View.GONE);
				TrackerData.setVisibility(View.GONE);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public class WaitForRestart extends AsyncTask<String, Void, Void>{

		@Override
		protected Void doInBackground(String... params) {

			try {
				Log.i("ROOOOOH", "Going to sleep for "+params[0]+" milliseconds");
				Thread.sleep(Integer.valueOf(params[0]));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.i("UUUUH", "Woke up");
			//Update your layout here
			super.onPostExecute(result);
			reloadApp("restart");
		}
	}


	@Override
	public void onInit(int status) {

		if (status == TextToSpeech.SUCCESS) {

			int result = tts.setLanguage(Locale.ENGLISH);

			if (result == TextToSpeech.LANG_MISSING_DATA|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("TTS", "This Language is not supported");
			} else {
				isttsOK=true;
			}

		} else {
			Log.e("TTS", "Initilization Failed!");
		}

	}


	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}
		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.AT_MOST);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0) {
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));
			}
			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	private void speakOut(String speech) {
		try {
			if(isttsOK) {
				if(tts!=null) {
					tts.setSpeechRate(0.65f);				
					tts.speak(speech, TextToSpeech.QUEUE_FLUSH, null);				

				}
			}
		}catch(Exception e) {
			e.printStackTrace();

		}
	}

	@SuppressWarnings("rawtypes")
	private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException {
		try {
			final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class.forName(conman.getClass().getName());
			final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
			connectivityManagerField.setAccessible(true);
			final Object connectivityManager = connectivityManagerField.get(conman);
			final Class connectivityManagerClass =  Class.forName(connectivityManager.getClass().getName());
			@SuppressWarnings("unchecked")
			final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);


			setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void registerSafeTrackerDaemon(String status) {
		try {
			GPSdataReceiver resultReceiver = new GPSdataReceiver(null);		
			Intent DaemonSpawnerIntent = new Intent(this,SafeTrackerDaemon.class);		
			DaemonSpawnerIntent.putExtra("receiver", resultReceiver);
			DaemonSpawnerIntent.putExtra("imei",IMEI);
			DaemonSpawnerIntent.putExtra("status",status);
			startService(DaemonSpawnerIntent);
			Toast.makeText(this, "Tracker service daemon spawned", Toast.LENGTH_LONG).show();
		}catch(Exception e) {
			e.printStackTrace();
			reloadApp("restart");
		}
	}

	public void getBobaProfile(String URL) {


		runOnUiThread(new Runnable() {

			public void run() 
			{
				new HTTPRequestTimerOut("profile",15000).start();
			}
		});
		getProfileTask = new AsyncTask<String,String,String>(){
			String query=" ",message="could not retrieve data";
			String hasNewMessage="failed";
			HttpURLConnection urlConnection;
			@Override
			protected void onPreExecute() {

				runOnUiThread(new Runnable() {

					public void run() 
					{
						toggleButtons("lock");
						showLoading();

					}

				});
			}

			@Override
			protected void onCancelled(){
				Log.e("BRUTAL","getBobaProfile has been timed out");
				try {
					if(urlConnection!=null) {
						urlConnection.disconnect();
						urlConnection=null;
						Log.e("BRUTAL","getBobaProfile urlConnection.disconnect();t");
					}else {
						Log.e("BRUTAL","getBobaProfile urlConnection.disconnect() was null");
					}
					hideLoading();
					if("success".equalsIgnoreCase(query)) {
						message="Saybo  "+SafeBodaName+" , Welcome to SafeBoda";
						if(bodaID!=null&&Integer.valueOf(bodaID)>=1) {							
							Picasso.with(SafeActivityContext)
							.load(Pic)
							.error(R.drawable.ic_launcher)
							.placeholder(R.drawable.holder)
							.into(bodaPic);
							BodaName.setText(SafeBodaName);
							if("success".equals(hasNewMessage)) {
								if(Integer.parseInt(NewMessageCount)>=1) {
									MsgCount.setText(NewMessageCount);									
								}
							}
						}else {		
							runOnUiThread(new Runnable() {							
								@SuppressWarnings("deprecation")
								public void run() 
								{
									final AlertDialog.Builder	alertbox = new AlertDialog.Builder(SafeActivityContext);            	            
									alertbox.setTitle("failed");
									alertbox.setCancelable(false);
									alertbox.setMessage("Could not retrieve your profile...\n Make sure your device is registered with SafeBoda.");
									alertbox.setNeutralButton("RESTART", new DialogInterface.OnClickListener() {	                               		
										@Override
										public void onClick(DialogInterface arg0, int arg1) {
											arg0.dismiss();
											reloadApp("restart");
										}
									});	

									alertbox.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {	                               		
										@Override
										public void onClick(DialogInterface arg0, int arg1) {
											arg0.dismiss();								
										}
									});
									AlertDialog alert = alertbox.create();
									alert.show();
									//retrieving the button view in order to handle it.

									Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
									Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


									if (neutral_button != null) {
										neutral_button.setBackgroundDrawable(SafeActivityContext.getResources()
												.getDrawable(R.drawable.app_button_background));

										neutral_button.setTextColor(SafeActivityContext.getResources()
												.getColor(android.R.color.white));
									}	

									if (positive_button != null) {
										positive_button.setBackgroundDrawable(SafeActivityContext.getResources()
												.getDrawable(R.drawable.app_button_background));

										positive_button.setTextColor(SafeActivityContext.getResources()
												.getColor(android.R.color.white));
									}	
								}

							});
						}

					}else {
						runOnUiThread(new Runnable() {							
							@SuppressWarnings("deprecation")
							public void run() 
							{									
								final AlertDialog.Builder	alertbox = new AlertDialog.Builder(SafeActivityContext);   
								alertbox.setCancelable(false);
								alertbox.setTitle("failed");
								if(message!=null&&message.length()>4) {
									alertbox.setMessage(message+"\n we will restart app!");
								}else {
									message="Hello Boda, We could not retrieve your profile! Click cancel if it's your first time or restart otherwise..";
									alertbox.setMessage("Hello Boda,\nWe could not retrieve your profile!\nClick cancel if it's your first time or restart otherwise...");
								}
								alertbox.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {	                               		
									@Override
									public void onClick(DialogInterface arg0, int arg1) {
										arg0.dismiss();										
									}
								});	

								alertbox.setPositiveButton("RESTART", new DialogInterface.OnClickListener() {	                               		
									@Override
									public void onClick(DialogInterface arg0, int arg1) {
										arg0.dismiss();
										reloadApp("restart");
									}
								});	
								AlertDialog alert = alertbox.create();
								alert.show();
								//retrieving the button view in order to handle it.

								Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
								Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


								if (neutral_button != null) {
									neutral_button.setBackgroundDrawable(SafeActivityContext.getResources()
											.getDrawable(R.drawable.app_button_background));

									neutral_button.setTextColor(SafeActivityContext.getResources()
											.getColor(android.R.color.white));
								}

								if (positive_button != null) {
									positive_button.setBackgroundDrawable(SafeActivityContext.getResources()
											.getDrawable(R.drawable.app_button_background));

									positive_button.setTextColor(SafeActivityContext.getResources()
											.getColor(android.R.color.white));
								}

							}

						});

					}

				}catch(Exception e) {
					e.printStackTrace();
				}

				refreshStatusControls(query,message,"noshow");
				populateListView("mission");

			}

			@Override
			protected String doInBackground(String... params) {
				String response = "";
				try{
					URL url = new URL(params[0]);
					urlConnection = (HttpURLConnection)url.openConnection();
					if(urlConnection!=null) {
						urlConnection.addRequestProperty("User-Agent",IMEI);					

					}
					BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String line = "";					
					while((line = reader.readLine()) != null){
						response += line + "\n";			
					}               
					if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
						Log.e("Bad Content","Received bad content type while expecting json");
						return null;
					}


					JSONObject obj = new JSONObject(response.toString());	                                
					query=obj.getString("status");					
					if("success".equalsIgnoreCase(query)) {
						JSONObject data = new JSONObject(obj.getString("bodadata"));
						String Json_fname=data.getString("first_name");
						String Json_lname=data.getString("last_name");
						String Json_picname=data.getString("avatar");
						bodaID=data.getString("id");
						if(Json_fname!=null&&Json_fname.length()>=2&&Json_lname!=null&&Json_lname.length()>=2) {
							SafeBodaName=Json_fname+" "+Json_lname;
						}
						if(Json_picname!=null&&Json_picname.length()>=20) {
							Json_picname.replace("./","/");
							Pic=HOST+Json_picname;
						}
						JSONObject NewMessageData = new JSONObject(obj.getString("message"));
						String bodaMotion=data.getString("status");
						if(bodaMotion!=null) {
							if("-2".equals(bodaMotion)) {
								BodaStatus="Away";

							}else if("-1".equals(bodaMotion)) {
								BodaStatus="Busy";

							}else if("-0".equals(bodaMotion)) {
								BodaStatus="Available";

							}else if("0".equals(bodaMotion)) {
								BodaStatus="Available";

							}else if("1".equals(bodaMotion)) {
								BodaStatus="Busy";

							}else if("2".equals(bodaMotion)) {
								BodaStatus="Away";

							}else {
								BodaStatus="undefined";
							}

						}else {
							BodaStatus="undefined";
						}

						hasNewMessage=NewMessageData.getString("status");
						if("success".equalsIgnoreCase(hasNewMessage)) {
							NewMessageCount=NewMessageData.getString("count");																														
						}
					}else {
						message=obj.getString("bodadata");
					}

					JSONArray jsonArry=new JSONArray(obj.getString("mission"));	
					if(jsonArry.length()>=1) {
						MissionAuthor=new ArrayList<String>();
						MissionDate=new ArrayList<String>();
						MissionID=new ArrayList<String>();
						MissionBody=new ArrayList<String>();
						MissionLat=new ArrayList<String>();
						MissionLon=new ArrayList<String>();

						for(int i=0 ;i<jsonArry.length();i++){
							JSONObject mission=jsonArry.getJSONObject(i);
							MissionAuthor.add(mission.getString("sender"));
							MissionDate.add(mission.getString("time"));
							MissionID.add(mission.getString("id"));
							MissionBody.add(mission.getString("extra"));
							try {
								String coord=mission.getString("locations");
								if(coord!=null) {
									String[] Coordinates=coord.split(",");
									if(Coordinates!=null&&Coordinates.length==2) {
										MissionLat.add(Coordinates[0]);
										MissionLon.add(Coordinates[1]);
									}else {
										MissionLat.add("0.0");
										MissionLon.add("100.78");
									}
								}else {
									MissionLat.add("0.0");
									MissionLon.add("100.78");
								}
							}catch(Exception e) {
								e.printStackTrace();
								MissionLat.add("0.0");
								MissionLon.add("100.78");
							}
						}
					}

				}
				catch (Exception e){
					Log.e("EXC","An exception occured while getting boda data"+e.getMessage());	
					return null;
				}				
				return response;


			}

			@SuppressWarnings("deprecation")
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					hideLoading();
					if("success".equalsIgnoreCase(query)) {
						message="Saybo "+SafeBodaName+" , Welcome to SafeBoda !";
						if(bodaID!=null&&Integer.valueOf(bodaID)>=1) {							
							Picasso.with(SafeActivityContext)
							.load(Pic)
							.error(R.drawable.ic_launcher)
							.placeholder(R.drawable.holder)
							.into(bodaPic);
							BodaName.setText(SafeBodaName);
							if("success".equals(hasNewMessage)) {
								if(Integer.parseInt(NewMessageCount)>=1) {
									MsgCount.setText(NewMessageCount);									
								}
							}
						}else {		
							runOnUiThread(new Runnable() {							
								public void run() 
								{
									final AlertDialog.Builder	alertbox = new AlertDialog.Builder(SafeActivityContext);            	            
									alertbox.setTitle("failed");
									alertbox.setCancelable(false);
									alertbox.setMessage("Could not retrieve your profile...\n Make sure your device is registered with SafeBoda.");
									alertbox.setNeutralButton("RESTART", new DialogInterface.OnClickListener() {	                               		
										@Override
										public void onClick(DialogInterface arg0, int arg1) {
											arg0.dismiss();
											reloadApp("restart");
										}
									});	

									alertbox.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {	                               		
										@Override
										public void onClick(DialogInterface arg0, int arg1) {
											arg0.dismiss();								
										}
									});
									AlertDialog alert = alertbox.create();
									alert.show();
									//retrieving the button view in order to handle it.

									Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
									Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


									if (neutral_button != null) {
										neutral_button.setBackgroundDrawable(SafeActivityContext.getResources()
												.getDrawable(R.drawable.app_button_background));

										neutral_button.setTextColor(SafeActivityContext.getResources()
												.getColor(android.R.color.white));
									}	

									if (positive_button != null) {
										positive_button.setBackgroundDrawable(SafeActivityContext.getResources()
												.getDrawable(R.drawable.app_button_background));

										positive_button.setTextColor(SafeActivityContext.getResources()
												.getColor(android.R.color.white));
									}	
								}

							});
						}

					}else {
						runOnUiThread(new Runnable() {							
							public void run() 
							{									
								final AlertDialog.Builder	alertbox = new AlertDialog.Builder(SafeActivityContext);   
								alertbox.setCancelable(false);
								alertbox.setTitle("failed");
								if(message!=null&&message.length()>4) {
									alertbox.setMessage(message+"\n we will restart app!");
								}else {
									message="Hello Boda, We could not retrieve your profile! Click cancel if it's your first time or restart otherwise..";
									alertbox.setMessage("Hello Boda,\nWe could not retrieve your profile!\nClick cancel if it's your first time or restart otherwise...");
								}
								alertbox.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {	                               		
									@Override
									public void onClick(DialogInterface arg0, int arg1) {
										arg0.dismiss();										
									}
								});	

								alertbox.setPositiveButton("RESTART", new DialogInterface.OnClickListener() {	                               		
									@Override
									public void onClick(DialogInterface arg0, int arg1) {
										arg0.dismiss();
										reloadApp("restart");
									}
								});	
								AlertDialog alert = alertbox.create();
								alert.show();
								//retrieving the button view in order to handle it.

								Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
								Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);


								if (neutral_button != null) {
									neutral_button.setBackgroundDrawable(SafeActivityContext.getResources()
											.getDrawable(R.drawable.app_button_background));

									neutral_button.setTextColor(SafeActivityContext.getResources()
											.getColor(android.R.color.white));
								}

								if (positive_button != null) {
									positive_button.setBackgroundDrawable(SafeActivityContext.getResources()
											.getDrawable(R.drawable.app_button_background));

									positive_button.setTextColor(SafeActivityContext.getResources()
											.getColor(android.R.color.white));
								}

							}

						});

					}

				}catch(Exception e) {
					e.printStackTrace();
				}

				refreshStatusControls(query,message,"noshow");
				populateListView("mission");
			}
		};
		getProfileTask.execute(URL);
	} 

	@SuppressWarnings("deprecation")
	private void refreshStatusControls(String Query,String message,String mode) {
		try {

			if("Available".equalsIgnoreCase(BodaStatus)) {
				toggleButtons("on");
				registerSafeTrackerDaemon("on");
			}else if("Busy".equalsIgnoreCase(BodaStatus)) {
				registerSafeTrackerDaemon("on");
				toggleButtons("busy");
			}else if("Away".equalsIgnoreCase(BodaStatus)) {
				registerSafeTrackerDaemon("off");
				toggleButtons("off");
			}else {
				registerSafeTrackerDaemon("dead");	
				
			}

			Log.e("SHITTY STATUS",BodaStatus);
			Vibrator v = (Vibrator) SafeActivityContext.getSystemService(Context.VIBRATOR_SERVICE);	
			if("keep".equalsIgnoreCase(Query)){

			}else if("success".equalsIgnoreCase(Query)){
				if("available".equalsIgnoreCase(BodaStatus)){	
					v.vibrate(1000);	
					BodaStatus="available";
				}else if("busy".equalsIgnoreCase(BodaStatus)) {
					long pattern[] = { 0,1000, 1000,1000};
					v.vibrate(pattern,-1);	
					BodaStatus="busy";
				}else if("away".equalsIgnoreCase(BodaStatus)){
					long pattern[] = { 0,1000, 1000,1000, 1000,1000};
					v.vibrate(pattern,-1);	
					BodaStatus="away";
				}else {
					toggleButtons("all");
					if(!"noshow".equalsIgnoreCase(mode)) {
						if(overrideCount<=3) {
							long pattern[] = { 0,1000, 500, 1000,500,1000,500,1000};
							v.vibrate(pattern,-1);
							registerSafeTrackerDaemon("dead");
							final AlertDialog.Builder	alertbox = new AlertDialog.Builder(SafeActivityContext);        
							alertbox.setCancelable(false);
							alertbox.setTitle("failed");
							alertbox.setMessage("\tLost connection...\n\tCheck your internet bundle ?");
							alertbox.setNeutralButton("RESTART", new DialogInterface.OnClickListener() {	                               		
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									arg0.dismiss();
									reloadApp("restart");
								}
							});	

							alertbox.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {	                               		
								@Override
								public void onClick(DialogInterface arg0, int arg1) {

									try {		
										overrideCount++;
										OverrideBodaStatus(HOST+"/override/boda/"+bodaID+"/"+OverrideStatus);
										arg0.dismiss();	
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} 
								}
							});

							AlertDialog alert = alertbox.create();
							alert.show();
							//retrieving the button view in order to handle it.

							Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
							Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);

							if (neutral_button != null) {
								neutral_button.setBackgroundDrawable(SafeActivityContext.getResources()
										.getDrawable(R.drawable.app_button_background));

								neutral_button.setTextColor(SafeActivityContext.getResources()
										.getColor(android.R.color.white));
							}	

							if (positive_button != null) {
								positive_button.setBackgroundDrawable(SafeActivityContext.getResources()
										.getDrawable(R.drawable.app_button_background));

								positive_button.setTextColor(SafeActivityContext.getResources()
										.getColor(android.R.color.white));
							}	
							speakOut(message);
						}else {
							Toast.makeText(SafeActivityContext,"Sorry boda, it looks like you are out internet!",Toast.LENGTH_LONG).show();
							onExitSafeApp();
						}
					}				
				}
			}else {
				toggleButtons("all");
				if(!"noshow".equalsIgnoreCase(mode)) {
					if(overrideCount<=3) {
						long pattern[] = { 0,1000, 500, 1000,500,1000,500,1000};
						v.vibrate(pattern,-1);
						registerSafeTrackerDaemon("dead");
						final AlertDialog.Builder	alertbox = new AlertDialog.Builder(SafeActivityContext);        
						alertbox.setCancelable(false);
						alertbox.setTitle("failed");
						alertbox.setMessage("Lost connection...\n Check your internet bundle ?");
						alertbox.setNeutralButton("RESTART", new DialogInterface.OnClickListener() {	                               		
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								arg0.dismiss();
								overrideCount=0;
								reloadApp("restart");
							}
						});	

						alertbox.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {	                               		
							@Override
							public void onClick(DialogInterface arg0, int arg1) {

								try {		
									overrideCount++;
									OverrideBodaStatus(HOST+"/override/boda/"+bodaID+"/"+OverrideStatus);
									arg0.dismiss();	
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} 
							}
						});

						AlertDialog alert = alertbox.create();
						alert.show();
						//retrieving the button view in order to handle it.

						Button neutral_button = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
						Button positive_button = alert.getButton(DialogInterface.BUTTON_POSITIVE);

						if (neutral_button != null) {
							neutral_button.setBackgroundDrawable(SafeActivityContext.getResources()
									.getDrawable(R.drawable.app_button_background));

							neutral_button.setTextColor(SafeActivityContext.getResources()
									.getColor(android.R.color.white));
						}	

						if (positive_button != null) {
							positive_button.setBackgroundDrawable(SafeActivityContext.getResources()
									.getDrawable(R.drawable.app_button_background));

							positive_button.setTextColor(SafeActivityContext.getResources()
									.getColor(android.R.color.white));
						}	
						speakOut(message);
					}else {
						Toast.makeText(SafeActivityContext,"Sorry boda, it looks like you are out internet!",Toast.LENGTH_LONG).show();
						onExitSafeApp();
					}
				}

			}			
			speakOut(message);
		}catch(Exception e) {
			e.printStackTrace();
			Toast.makeText(SafeActivityContext,"We could not connect to safeserver",Toast.LENGTH_LONG).show();
			message="We could not make request to safeserver "+e.getMessage();
			speakOut(message);
		}
	}

	private void populateListView(String list) {
		try {
			if("mission".equals(list)) {
				ArrayList<Mission> missions =getPolledMissions();
				if(missions!=null&&missions.size()>=1) {								
					if(MissionList!=null){
						ListViewAdapter=new MissionListAdapter(bodaID,SafeActivityContext, missions);															
						MissionList.setAdapter(ListViewAdapter);
						setListViewHeightBasedOnChildren(MissionList);
					}
					NewMissionCount=""+missions.size();
				}
				MissionCount.setText(NewMissionCount);
			}else if("message".equals(list)) {
				ArrayList<Message> messages =getPolledMessages();
				if(messages!=null&&messages.size()>=1) {								
					if(MessageList!=null){
						MessageViewAdapter=new MessageListAdapter(bodaID,SafeActivityContext, messages);															
						MessageList.setAdapter(MessageViewAdapter);
						setListViewHeightBasedOnChildren(MessageList);
					}
					NewMessageCount=""+messages.size();
				}
				MsgCount.setText(NewMessageCount);
			}
		}catch(Exception e) {
			Toast.makeText(SafeActivityContext,"We could not refresh messages",Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	public static void rePopulateListView(String list,int item) {
		try {
			if("mission".equals(list)) {
				if(MissionAuthor!=null) {
					MissionAuthor.remove(item);
					MissionDate.remove(item);
					MissionID.remove(item);
					MissionBody.remove(item);
					ArrayList<Mission> missions =getPolledMissions();
					if(missions!=null) {								
						if(MissionList!=null){										
							ListViewAdapter=new MissionListAdapter(bodaID,SafeActivityContext, missions);																
							MissionList.setAdapter(ListViewAdapter);
							setListViewHeightBasedOnChildren(MissionList);
						}else {
							Toast.makeText(SafeActivityContext,"We could not refresh missions",Toast.LENGTH_LONG).show();
						}
						NewMissionCount=""+missions.size();
					}else {
						Toast.makeText(SafeActivityContext,"We could not refresh missions",Toast.LENGTH_LONG).show();
					}
					MissionCount.setText(NewMissionCount);
				}else {
					Toast.makeText(SafeActivityContext,"We could not refresh missions",Toast.LENGTH_LONG).show();
				}
			}else if("message".equals(list)) {
				if(MessageAuthor!=null) {
					MessageAuthor.remove(item);
					MessageDate.remove(item);
					MessageID.remove(item);
					MessageBody.remove(item);
					ArrayList<Message> Messages =getPolledMessages();
					if(Messages!=null) {								
						if(MessageList!=null){										
							MessageViewAdapter=new MessageListAdapter(bodaID,SafeActivityContext, Messages);																
							MessageList.setAdapter(MessageViewAdapter);
							setListViewHeightBasedOnChildren(MessageList);
						}else {
							Toast.makeText(SafeActivityContext,"We could not refresh messages",Toast.LENGTH_LONG).show();
						}
						NewMessageCount=""+Messages.size();
					}else {
						Toast.makeText(SafeActivityContext,"We could not refresh messages",Toast.LENGTH_LONG).show();
					}
					MsgCount.setText(NewMessageCount);
				}else {
					Toast.makeText(SafeActivityContext,"We could not refresh messages",Toast.LENGTH_LONG).show();
				}		
			}
		}catch(Exception e) {
			Toast.makeText(SafeActivityContext,"We could not refresh messages..."+e.getMessage(),Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}


	private void toggleButtons(String button) {
		try {
			if("all".equals(button)) {
				BusyConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OffConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OnConfirm.setTextColor(Color.argb(255, 73, 86,94));	

				OffConfirm.setBackgroundResource(R.drawable.off);
				BusyConfirm.setBackgroundResource(R.drawable.busy);
				OnConfirm.setBackgroundResource(R.drawable.on);

				OnConfirm.setEnabled(true);
				BusyConfirm.setEnabled(true);
				OffConfirm.setEnabled(true);

			}else if("on".equals(button)) {
				BusyConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OffConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OnConfirm.setTextColor(Color.argb(255, 255, 255,255));

				OffConfirm.setBackgroundResource(R.drawable.off);
				BusyConfirm.setBackgroundResource(R.drawable.busy);
				OnConfirm.setBackgroundResource(R.drawable.on_pressed);

				OnConfirm.setEnabled(false);		
				BusyConfirm.setEnabled(true);
				OffConfirm.setEnabled(true);
			}else if("off".equals(button)) {
				BusyConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OffConfirm.setTextColor(Color.argb(255, 255, 255,255));
				OnConfirm.setTextColor(Color.argb(255, 73, 86,94));	

				OffConfirm.setBackgroundResource(R.drawable.off_pressed);
				BusyConfirm.setBackgroundResource(R.drawable.busy);
				OnConfirm.setBackgroundResource(R.drawable.on);

				OnConfirm.setEnabled(true);
				BusyConfirm.setEnabled(true);							
				OffConfirm.setEnabled(false);

			}else if("busy".equals(button)) {
				BusyConfirm.setTextColor(Color.argb(255, 255, 255,255));
				OffConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OnConfirm.setTextColor(Color.argb(255, 73, 86,94));	

				OffConfirm.setBackgroundResource(R.drawable.off);
				BusyConfirm.setBackgroundResource(R.drawable.busy_pressed);
				OnConfirm.setBackgroundResource(R.drawable.on);

				OnConfirm.setEnabled(true);			
				OffConfirm.setEnabled(true);				
				BusyConfirm.setEnabled(false);
			}else {
				BusyConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OffConfirm.setTextColor(Color.argb(255, 73, 86,94));
				OnConfirm.setTextColor(Color.argb(255, 73, 86,94));	
				OffConfirm.setBackgroundResource(R.drawable.off);
				BusyConfirm.setBackgroundResource(R.drawable.busy);
				OnConfirm.setBackgroundResource(R.drawable.on);
				OnConfirm.setEnabled(false);
				BusyConfirm.setEnabled(false);
				OffConfirm.setEnabled(false);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}


	public void addListenerOnButton() {
		try {
			final Vibrator Shakor = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);			


			OnConfirm.setOnLongClickListener(new View.OnLongClickListener() {
				public boolean onLongClick(View v) {
					Shakor.vibrate(500);
					BodaStatus="Available";
					OverrideStatus="Available";
					overrideCount=0;
					OverrideBodaStatus(HOST+"/override/boda/"+bodaID+"/available");
					Log.v("ON CLICK", "about to go busy....button long pressed");									
					return true;
				}
			});


			BusyConfirm.setOnLongClickListener(new View.OnLongClickListener() {
				public boolean onLongClick(View v) {
					Shakor.vibrate(1000);
					BodaStatus="Busy";
					OverrideStatus="Busy";
					overrideCount=0;
					OverrideBodaStatus(HOST+"/override/boda/"+bodaID+"/busy");
					Log.v("BUSY CLICK", "about to go busy....button long pressed");					
					return true;
				}
			});


			OffConfirm.setOnLongClickListener(new View.OnLongClickListener() {
				public boolean onLongClick(View v) {
					Shakor.vibrate(1500);
					BodaStatus="Away";
					OverrideStatus="Away";
					overrideCount=0;
					OverrideBodaStatus(HOST+"/override/boda/"+bodaID+"/away");					
					Log.v("AWAY CLICK", "about to go away....button long pressed");							
					return true;
				}
			});
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static ArrayList<Mission> getPolledMissions() {
		ArrayList<Mission> results = new ArrayList<Mission>();
		try {
			for(int i=0;i<MissionID.size();i++) {
				Mission currentMission = new Mission();	
				currentMission.setmission(MissionBody.get(i));
				currentMission.setmissionDate(MissionDate.get(i));
				currentMission.setmissionSender(MissionAuthor.get(i));
				currentMission.setmissionStatus("0");
				currentMission.setmissionID(MissionID.get(i));
				currentMission.setmissionEndLat(MissionLat.get(i));
				currentMission.setmissionEndLong(MissionLon.get(i));
				results.add(currentMission);	
			}

		} catch (Exception exc) {
			exc.printStackTrace();
			return null;
		}

		return results;
	}


	private static ArrayList<Message> getPolledMessages() {
		ArrayList<Message> results = new ArrayList<Message>();
		try {
			for(int i=0;i<MessageID.size();i++) {
				Message currentMessage = new Message();	
				currentMessage.setMessage(MessageBody.get(i));
				currentMessage.setMessageDate(MessageDate.get(i));
				currentMessage.setMessageSender(MessageAuthor.get(i));
				currentMessage.setMessageSenderImage(MessageAuthorIcon.get(i));
				currentMessage.setMessageStatus("0");
				currentMessage.setMessageID(MessageID.get(i));			
				results.add(currentMessage);	
			}

		} catch (Exception exc) {
			exc.printStackTrace();
			return null;

		}

		return results;
	}


	private void registerExpirationTimer(int length) {
		try {
			if(expire!=null) {
				expire.cancel();
			}
			expire = new CountDownTimer(length,1000) {
				public void onTick(long millisUntilFinished) {

				}
				public void onFinish() {
					ExitCountTap=0;
					expire.cancel();

				}
			};
			expire.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void onDestroy(){
		this.finish();
		super.onDestroy();		
	}

	private void onExitSafeApp() {
		try {
			if(expire!=null) {
				expire.cancel();
			}
			if(this.updatetimer!=null) {
				updatetimer.cancel();
			}
			if(getProfileTask!=null) {
				getProfileTask.cancel(true);
			}

			if(BodaOverrider!=null) {
				BodaOverrider.cancel(true);
			}

			if(getMessageTask!=null) {
				getMessageTask.cancel(true);
			}

			if(getBodaStatus!=null) {
				getBodaStatus.cancel(true);
			}

			OnConfirm=null;
			bodaPic=null;
			BodaName=null;Latitude=null;Longitude=null;
			if(ListViewAdapter!=null) {
				ListViewAdapter.onExitactivity();
				ListViewAdapter=null;
			}

			SafeBodaName=null;Lat=null;Long=null;Pic=null;
			SafeActivityContext=null;
			if(Location!=null) {
				Location.stopUsingGPS();
				Location=null;
			}	
			updatetimer=null;
			expire=null;
			ExitCountTap=0;
			TapShow=null;
			if(tts!=null) {
				if(tts.isSpeaking()) {
					tts.stop();
				}
				tts.shutdown();
				tts=null;
			}
			System.gc();

			onDestroy();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}


	private void updateBodaMessages(String mode) {


		runOnUiThread(new Runnable() {

			public void run() 
			{
				new HTTPRequestTimerOut("message",12000).start();
			}
		});
		getMessageTask = new AsyncTask<String,String,String>(){
			String query,Message;		
			HttpURLConnection urlConnection;
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				try {
					query="failed";
					Message="could not connect to SafeServer\n";
					runOnUiThread(new Runnable() {

						public void run() 
						{
							showLoading();

						}

					});
				}catch(Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onCancelled(){
				try {
					Log.e("BRUTAL","updateBodaMessages has been timed out");
					hideLoading();
					if(urlConnection!=null) {
						urlConnection.disconnect();
						urlConnection=null;
					}
					runOnUiThread(new Runnable() {

						public void run() 
						{
							populateListView("message");	
						}

					});
				}catch(Exception e) {
					e.printStackTrace();

				}
			}

			@Override
			protected String doInBackground(String... params) {
				String response = "";

				try{
					URL url = new URL(params[0].trim());
					Log.e("STATUS","url "+params[0]);	
					urlConnection = (HttpURLConnection)url.openConnection();

					if(urlConnection!=null) {
						urlConnection.addRequestProperty("User-Agent",IMEI);											
					}
					int status = urlConnection.getResponseCode();

					if(status >= HttpStatus.SC_BAD_REQUEST) {
						Message+= urlConnection.getErrorStream().toString();
						return Message;
					}			

					BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String line = "";
					while((line = reader.readLine()) != null){
						response += line + "\n";
					}               
					if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
						Log.e("Bad Content","Received bad content type while expecting sjon");
					}

					Log.e("RESP BODY","body responded "+response);
					Log.e("RESP BODY","header agent "+urlConnection.getRequestProperty("User-Agent"));
					try {     	  
						JSONObject obj = new JSONObject(response.toString());	                
						query=obj.getString("status");
						if("success".equalsIgnoreCase(query)) {
							JSONArray jsonArry=new JSONArray(obj.getString("message"));	
							if(jsonArry.length()>=1) {
								MessageAuthor=new ArrayList<String>();
								MessageAuthorIcon=new ArrayList<String>();
								MessageDate=new ArrayList<String>();
								MessageID=new ArrayList<String>();
								MessageBody=new ArrayList<String>();
								for(int i=0 ;i<jsonArry.length();i++){
									JSONObject comment=jsonArry.getJSONObject(i);
									MessageAuthor.add(comment.getString("senderID"));
									MessageDate.add(comment.getString("message_date"));
									MessageAuthorIcon.add(HOST+"/image/customer/"+comment.getString("senderID"));
									MessageID.add(comment.getString("id"));
									MessageBody.add(comment.getString("message"));							
								}
								System.gc();
							}
						}else {
							Message=obj.getString("message");
						}
					}catch(JSONException exc) {
						Log.d("EXCEPTION ","something bad to JSOn happen "+exc.getMessage());
						return null;
					}
				}
				catch (Exception e){
					Log.e("EXC","An eception occured while getting data "+e.getMessage());	
					e.printStackTrace();
					return null;
				}

				return response;
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					hideLoading();

					runOnUiThread(new Runnable() {

						public void run() 
						{
							populateListView("message");	
						}

					});
				}catch(Exception e) {
					e.printStackTrace();

				}
			}
		};
		getMessageTask.execute(HOST+"/data/boda/message/"+bodaID+"/"+mode);	
	}


	public void OverrideBodaStatus(String path) {

		runOnUiThread(new Runnable() {

			public void run() 
			{
				new HTTPRequestTimerOut("override",12000).start();
			}
		});

		BodaOverrider = new AsyncTask<String,String,String>(){

			String query,Message;
			HttpURLConnection urlConnection;
			@Override
			protected void onPreExecute() {
				super.onPreExecute();	
				try {

					query="failed";
					Message="could not connect to SafeServer\n";

					runOnUiThread(new Runnable() {

						public void run() 
						{
							toggleButtons("lock");	
							showLoading();


						}

					});
				}catch(Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onCancelled(){
				try {
					Log.e("BRUTAL","OverrideBodaStatus has been timed out");
					if(urlConnection!=null) {
						urlConnection.disconnect();
						urlConnection=null;
					}
					runOnUiThread(new Runnable() {

						public void run() 
						{

							hideLoading();

							refreshStatusControls(query,Message,"show");
						}

					});
				}catch(Exception e) {
					e.printStackTrace();
				}
			}


			@Override
			protected String doInBackground(String... params) {
				String response = "";
				try{
					URL url = new URL(params[0].trim());
					Log.e("STATUS","url "+params[0]);	

					urlConnection = (HttpURLConnection)url.openConnection();

					if(urlConnection!=null) {
						urlConnection.addRequestProperty("User-Agent",IMEI);						
					}

					int status = urlConnection.getResponseCode();

					if(status >= HttpStatus.SC_BAD_REQUEST) {
						Message+= urlConnection.getErrorStream().toString();
						return Message;
					}			

					BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String line = "";
					while((line = reader.readLine()) != null){
						response += line + "\n";
					}               
					if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
						Log.e("Bad Content","Received bad content type while expecting sjon");
					}

					Log.e("RESP BODY","body responded "+response);
					Log.e("RESP BODY","header agent "+urlConnection.getRequestProperty("User-Agent"));
					try {     	  
						JSONObject obj = new JSONObject(response.toString());	                
						query=obj.getString("status");
						Message=obj.getString("motion");						
					}catch(JSONException exc) {
						Log.d("EXCEPTION ","something bad to JSOn happen "+exc.getMessage());
						return null;
					}
				}
				catch (Exception e){
					Log.e("EXC","An eception occured while getting data "+e.getMessage());	
					e.printStackTrace();
					return null;
				}


				return response;


			}

			@Override
			protected void onPostExecute(String result) {
				try {

					runOnUiThread(new Runnable() {

						public void run() 
						{
							hideLoading();

							refreshStatusControls(query,Message,"show");
						}

					});
				}catch(Exception e) {
					e.printStackTrace();

				}

			}

		};
		BodaOverrider.execute(path);
	}


	public void getBodaStatus(String path) {

		runOnUiThread(new Runnable() {

			public void run() 
			{
				new HTTPRequestTimerOut("status",12000).start();
			}
		});

		getBodaStatus = new AsyncTask<String,String,String>(){

			String query,hasNewMessage;
			HttpURLConnection urlConnection;
			@Override
			protected void onCancelled(){
				Log.e("BRUTAL","getBodaStatus has been timed out");
				try {
					if(urlConnection!=null) {
						urlConnection.disconnect();
						urlConnection=null;
					}
					runOnUiThread(new Runnable() {

						public void run() 
						{		
							hideLoading();

							if("success".equalsIgnoreCase(hasNewMessage)){
								if(Integer.parseInt(NewMessageCount)>=1) {
									MsgCount.setText(NewMessageCount);									
								}
							}

							if("success".equals(query)) {
								if("-2".equals(BodaStatus)) {
									BodaStatus="Away";

								}else if("-1".equals(BodaStatus)) {
									BodaStatus="Busy";

								}else if("-0".equals(BodaStatus)) {
									BodaStatus="Available";

								}else if("0".equals(BodaStatus)) {
									BodaStatus="Available";

								}else if("1".equals(BodaStatus)) {
									BodaStatus="Busy";

								}else if("2".equals(BodaStatus)) {
									BodaStatus="Away";						
								}else {
									BodaStatus="undefined";
								}

								populateListView("mission");

								if("success".equalsIgnoreCase(hasNewMessage)) {
									updateBodaMessages("new");	
								}

								refreshStatusControls(query,BodaName+" your status has been updated to "+BodaStatus,"show");
							}else {
								refreshStatusControls("keep",BodaName+"We could not determine your status","noshow");
							}
						}
					});
				}catch(Exception e) {
					e.printStackTrace();

				}
			}

			@Override
			protected String doInBackground(String... params) {
				String response = "";
				try{
					URL url = new URL(params[0]);
					Log.e("STATUS","url "+params[0]);
					final  HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();	

					if(urlConnection!=null) {
						urlConnection.addRequestProperty("User-Agent",IMEI);
					}

					BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String line = "";
					while((line = reader.readLine()) != null){
						response += line + "\n";
					}               
					if(!urlConnection.getContentType().toLowerCase(Locale.ENGLISH).equals("application/json")) {
						Log.e("Bad Content","Received bad content type while expecting sjon");
					}

					Log.e("STATUS","body responded "+response);
					try {     	  
						JSONObject obj = new JSONObject(response.toString());	                
						query=obj.getString("status");
						if("success".equalsIgnoreCase(query)) {
							BodaStatus=obj.getString("motion");
							JSONObject NewMessageData = new JSONObject(obj.getString("message"));
							hasNewMessage=NewMessageData.getString("status");
							if("success".equalsIgnoreCase(hasNewMessage)) {
								NewMessageCount=NewMessageData.getString("count");																	
							}
						}

						JSONArray jsonArry=new JSONArray(obj.getString("mission"));	
						if(Integer.valueOf(NewMissionCount)<jsonArry.length()) {
							MissionAuthor=new ArrayList<String>();
							MissionDate=new ArrayList<String>();
							MissionID=new ArrayList<String>();
							MissionBody=new ArrayList<String>();
							for(int i=0 ;i<jsonArry.length();i++){
								JSONObject mission=jsonArry.getJSONObject(i);
								MissionAuthor.add(mission.getString("sender"));
								MissionDate.add(mission.getString("time"));
								MissionID.add(mission.getString("id"));
								MissionBody.add(mission.getString("extra"));
								try {
									String coord=mission.getString("locations");
									if(coord!=null) {
										String[] Coordinates=coord.split(",");
										if(Coordinates!=null&&Coordinates.length==2) {
											MissionLat.add(Coordinates[0]);
											MissionLon.add(Coordinates[1]);
										}else {
											MissionLat.add("0.0");
											MissionLon.add("100.78");
										}
									}else {
										MissionLat.add("0.0");
										MissionLon.add("100.78");
									}
								}catch(Exception e) {
									e.printStackTrace();
									MissionLat.add("0.0");
									MissionLon.add("100.78");
								}
							}
							System.gc();
						}
					}catch(JSONException exc) {
						Log.d("EXCEPTION ","somathing bad to JSOn happen "+exc.getMessage());
						return null;
					}

				}
				catch (Exception e){
					Log.e("EXC","An eception occured while getting data "+e.getMessage());	
					return null;
				}


				return response;


			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();	

				runOnUiThread(new Runnable() {

					public void run() 
					{
						toggleButtons("lock");
						showLoading();				
					}
				});
			}

			@Override
			protected void onPostExecute(String result) {
				try {

					runOnUiThread(new Runnable() {

						public void run() 
						{		
							hideLoading();

							if("success".equalsIgnoreCase(hasNewMessage)){
								if(Integer.parseInt(NewMessageCount)>=1) {
									MsgCount.setText(NewMessageCount);									
								}
							}

							if("success".equals(query)) {
								if("-2".equals(BodaStatus)) {
									BodaStatus="Away";

								}else if("-1".equals(BodaStatus)) {
									BodaStatus="Busy";

								}else if("-0".equals(BodaStatus)) {
									BodaStatus="Available";

								}else if("0".equals(BodaStatus)) {
									BodaStatus="Available";

								}else if("1".equals(BodaStatus)) {
									BodaStatus="Busy";

								}else if("2".equals(BodaStatus)) {
									BodaStatus="Away";						
								}

								populateListView("mission");

								if("success".equalsIgnoreCase(hasNewMessage)) {
									updateBodaMessages("new");	
								}

								refreshStatusControls(query,BodaName+" your status has been updated to "+BodaStatus,"show");
							}else {
								refreshStatusControls("keep",BodaName+"We could not determine your status","show");
							}
						}
					});
				}catch(Exception e) {
					e.printStackTrace();

				}

			}

		};
		getBodaStatus.execute(path);
	}


	class QueryBodaStatusTimerTask extends TimerTask
	{		
		@Override
		public void run() {
			Log.d("TACTAC","about to udate status timer");
			getBodaStatus(HOST+"/safetracker/boda/"+bodaID+"/status");
		}
	}

	class UpdateUI implements Runnable{

		String provider="none",speed="0.0",accuracy="100000.01",
				satCount="0",FixSatelliteCount="0", time="0", date="0";
		int result=0;
		public UpdateUI(final int resultCode,String provider,String lat,String longitude,String speed,String status,String satellites,
				String fixsatellites,String accuracy,String time,String date) {
			this.provider=provider;
			longit=longitude;
			latit=lat;
			this.speed=speed;
			this.provider=provider;
			this.result=resultCode;
			this.accuracy=accuracy;
			this.time=time;
			this.date=date;
			satCount=satellites;
			FixSatelliteCount=fixsatellites;

		}
		public void run() {
			try {
				if(Latitude!=null) {
					if("gps".equals(provider)) {
						Latitude.setTextColor(Color.argb(125,0,255,0));	
						Longitude.setTextColor(Color.argb(125,0,255,0));	
					}else if("net".equals(provider)) {
						Latitude.setTextColor(Color.argb(125,255,255,0));	
						Longitude.setTextColor(Color.argb(125,255,255,0));	
					}else {
						Latitude.setTextColor(Color.argb(125,255,0,0));	
						Longitude.setTextColor(Color.argb(125,255,0,0));	
					}

					if(Float.parseFloat(speed)>=2) {
						Speed.setTextColor(Color.argb(150,0,255,0));
					}else {
						Speed.setTextColor(Color.argb(150,255,0,0));
					}

					if(Float.parseFloat(accuracy)>=100f) {
						Accuracy.setTextColor(Color.argb(150,255,0,0));	
					}else {
						Accuracy.setTextColor(Color.argb(150,0,255,0));	
					}

					if(Integer.parseInt(FixSatelliteCount)>=4) {
						Satellites.setTextColor(Color.argb(150,0,255,0));	
					}else {
						Satellites.setTextColor(Color.argb(150,255,0,0));	
					}

					if(latit!=null&&!"0.0".equals(latit)) {
						if(latit.length()>11) {
							Latitude.setText(latit.substring(0,10));
						}else {
							Latitude.setText(latit);
						}
					}else {
						Latitude.setText("lost");
					}

					if(longit!=null&&!"0.0".equals(longit)) {
						if(longit.length()>11) {
							Longitude.setText(longit.substring(0,10));
						}else {
							Longitude.setText(longit);	
						}
					}else {
						Longitude.setText("lost");
					}

					if(satCount==null) {
						satCount="0";
					}

					if(FixSatelliteCount==null) {
						FixSatelliteCount="0";
					}

					Satellites.setText("Total :\t"+satCount+"\nIn fix :\t"+FixSatelliteCount);
					Speed.setText(speed+" (m/s)");		
					Accuracy.setText(accuracy+" (m)");
					GPStime.setText(date+"\n"+time);			
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}

	}

	class GPSdataReceiver extends ResultReceiver{
		public GPSdataReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			try {
				if(resultData!=null) {
					if(Integer.valueOf(resultData.getString("lockmiss"))>=10) {
						Log.d("RELOAD","restarting app and services");
						reloadApp("restart");
					}else {
						runOnUiThread(new UpdateUI(resultCode,resultData.getString("provider"),
								resultData.getString("lat"),resultData.getString("long"),
								resultData.getString("speed"),resultData.getString("status"),resultData.getString("sats" ),
								resultData.getString("fixsats" ),resultData.getString("accuracy"),resultData.getString("time"),resultData.getString("date")));
					}
				}else {
					System.out.println("There was an error...tracker daemon sent null");
				}

			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}


	@Override
	public void onPause(){
		try {
			if(tts !=null){
				tts.stop();
				tts.shutdown();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		super.onPause();
	}

	@Override
	public void onBackPressed(){
		Toast.makeText(this,"Button has been disabled.To hide press home or tap 10 times to profile image to exit app",Toast.LENGTH_LONG+Toast.LENGTH_LONG).show();
	}

	public  boolean hasConnection() {
		try {
			ConnectivityManager cm = (ConnectivityManager) SafeActivityContext.getSystemService(
					Context.CONNECTIVITY_SERVICE);

			NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (wifiNetwork != null && wifiNetwork.isConnected()) {
				return true;
			}

			NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (mobileNetwork != null && mobileNetwork.isConnected()) {
				return true;
			}

			NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			if (activeNetwork != null && activeNetwork.isConnected()) {
				return true;
			}

			return false;
		}catch(Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	private void showLoading() {
		try {
			if(pollPB!=null) {					
				pollPB.setVisibility(View.VISIBLE);
			}

			if(bodaPic!=null) {
				AlphaAnimation flashBodaImage = new AlphaAnimation(0f, 1.0f);
				flashBodaImage.setDuration(3000);
				flashBodaImage.setRepeatCount(Animation.INFINITE);
				bodaPic.startAnimation(flashBodaImage);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}


	private void hideLoading() {
		try {
			if(pollPB!=null) {					
				pollPB.setVisibility(View.INVISIBLE);
			}
			bodaPic.clearAnimation();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void sendScroll(){
		final Handler handler = new Handler();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {

					Thread.sleep(100);
				} catch (InterruptedException e) {}
				handler.post(new Runnable() {
					@Override
					public void run() {
						Parent.fullScroll(View.FOCUS_DOWN);
					}
				});
			}
		}).start();
	}

	private final class HTTPRequestTimerOut extends CountDownTimer {

		String ConnectionName;
		public HTTPRequestTimerOut( String current, long exp) {
			super(exp,1000);
			this.ConnectionName = current;
			Log.d("EXP TIMER","timer called");
		}       

		public void onFinish() {
			try {
				if("profile".equals(ConnectionName)){
					if(getProfileTask!=null) {
						getProfileTask.cancel(true);
						Log.e("EXP TIMER","getProfileTask timertask was aborted by timer");
					}else {
						Log.e("EXP TIMER","getProfileTask null");
					}

					System.gc();

				}else if("message".equals(ConnectionName)){
					if(getMessageTask!=null) {
						getMessageTask.cancel(true);
						Log.e("EXP TIMER","getMessageTask timertask was aborted by timer");
					}else {
						Log.e("EXP TIMER","getMessageTask null");
					}

					System.gc();

				}else if("override".equals(ConnectionName)){
					if(BodaOverrider!=null) {
						BodaOverrider.cancel(true);
						Log.e("EXP TIMER","BodaOverrider timertask was aborted by timer");
					}else {
						Log.e("EXP TIMER","BodaOverrider null");
					}

					System.gc();

				}if("status".equals(ConnectionName)){
					if(getBodaStatus!=null) {
						getBodaStatus.cancel(true);
						Log.e("EXP TIMER","getBodaStatus timertask was aborted by timer");
					}else {
						Log.e("EXP TIMER","getBodaStatus null");
					}

					System.gc();

				}

				this.cancel();

			}catch(Exception e) {
				e.printStackTrace();
			}
		}

		public void onTick(long millisUntilFinished) {
			Log.e("EXP TIMER","tick tick");	
		}
	}



	public void reloadApp(String mode) {
		try {
			if(BodaOverrider!=null) {
				BodaOverrider.cancel(true);
			}
			if(getProfileTask!=null) {
				getProfileTask.cancel(true);
			}

			if(getMessageTask!=null) {
				getMessageTask.cancel(true);
			}

			if(getBodaStatus!=null) {
				getBodaStatus.cancel(true);
			}

			Intent DaemonSpawnerIntent = new Intent(SafeActivityContext,SafeTrackerDaemon.class);						
			DaemonSpawnerIntent.putExtra("imei",IMEI);
			DaemonSpawnerIntent.putExtra("status","die");
			startService(DaemonSpawnerIntent);		
			Log.e("DAEMON","zombieing the safetracke daemon...");
			if(mode.equals("restart")) {
				Intent intent = getIntent();
				overridePendingTransition(0, 0);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				finish();
				overridePendingTransition(0, 0);
				startActivity(intent);	
			}else {
				onExitSafeApp();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
